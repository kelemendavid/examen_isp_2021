public class Ex1 {

    class B extends A{  //mostenire

        private String param;
        D d; //asociere
        //C c = new C(); alta varianta pentru compozitie
        E e;

        B(E parameterOfE){
            C c = new C(); // compozitie
            this.e = parameterOfE; //agregare
        }

        public void x(){

        }

        public void y(){

        }
    }

    class U{
        public void doSomething(B parameterOfB){
            B localVariableB;       //dependenta-variable locale sau parametru la metoda
        }
    }

    class A{

    }

    class D{

    }

    class E{

    }

    class C{

    }

}
