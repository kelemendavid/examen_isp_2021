import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Scanner;

public class Ex2 extends JFrame {

    JButton button;
    JLabel label;
    JTextArea textArea;

    String filePath = "C:\\Users\\David\\Desktop\\Examen ISP\\examen_isp_2021\\src\\main\\java\\";

    Ex2() throws IOException {
        setTitle("Ex1");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);

        button = new JButton("Apasa!");
        button.setBounds(250,10,100,30);
        button.addActionListener(new TratareButon());
        add(button);

        label = new JLabel("Nume fisier");
        label.setBounds(10,10,100,30);
        add(label);

        textArea = new JTextArea("");
        textArea.setBounds(100,10,100,30);
        add(textArea);



    }

    class TratareButon implements ActionListener {


        public void actionPerformed(ActionEvent e) {

            try {
                readFromFileReverseAndPrint(textArea.getText());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }


        }
    }

    void readFromFileReverseAndPrint(String fileName) throws IOException {
        StringBuilder data = new StringBuilder();
        try {

            File myObj = new File(filePath+fileName);
            Scanner myReader = new Scanner(myObj);

            while (myReader.hasNextLine()) {

                 data.append(myReader.nextLine()); //construim in String tot mesajul aflat in file,nu doar ultima linie

            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Something happened! File not found!");
            e.printStackTrace();
        }

        byte[] dataAsByteArray = data.toString().getBytes();
        byte[] result = new byte[dataAsByteArray.length];


        for (int i = 0; i < dataAsByteArray.length; i++)
            result[i] = dataAsByteArray[dataAsByteArray.length - i - 1];

        System.out.println(new String(result));

    }

    public static void main(String[] args) throws IOException {
        new Ex2();
    }
}
